# Docker PHP Redis

Simple test app running PHP app through Nginx server reading data from Redis using Docker containers.

## Usage

- Check you have Docker and Docker compose available on your machine
- Run `docker-compose up --build`
- Test the app is running on http://localhost:8080/
