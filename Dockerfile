FROM php:7.4-fpm

# Install necessary extensions
RUN pecl install redis && docker-php-ext-enable redis

WORKDIR /var/www/html

COPY src/ .

EXPOSE 9000